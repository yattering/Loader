@echo. Piosition-independed Loader by Yattering, 2016
@echo. e-mail: yattering (at) sigaint (d0t) org
@echo. jabber: yattering (at) xmpp (d0t) jp
@SET FASMINC=C:\DEV\FASM\INCLUDE
@echo. Compiling DLL...
@echo. output_format = 2 > settings.inc
@echo. __url equ 'https://gitlab.com/yattering/SEH_based_VM/raw/master/demo_fasm.exe' >> settings.inc
@echo. __file equ 'demo.exe' >> settings.inc
@C:\DEV\FASM\FASM.EXE loader.asm
@echo. Compiling EXE...
@echo. output_format = 1 > settings.inc
@echo. __url equ 'https://gitlab.com/yattering/SEH_based_VM/raw/master/demo_fasm.exe' >> settings.inc
@echo. __file equ 'demo.exe' >> settings.inc
@C:\DEV\FASM\FASM.EXE loader.asm
@echo. Compiling BIN...
@echo. output_format = 0 > settings.inc
@echo. __url equ 'https://gitlab.com/yattering/SEH_based_VM/raw/master/demo_fasm.exe' >> settings.inc
@echo. __file equ 'demo.exe' >> settings.inc
@C:\DEV\FASM\FASM.EXE loader.asm